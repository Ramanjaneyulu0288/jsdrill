const cars = require('./cars/cars')


const inventory = cars.inventory


const yearsOfCars = (inventory)=>{
    
    if(inventory==undefined) return 
if (!Array.isArray(inventory) ) return;
if(inventory.length == 0 ) return

    let years = []
    for (i = 0; i < inventory.length; i++){
        years.push(inventory[i].car_year)
    }

    return years
}

module.exports = yearsOfCars


// console.log(yearsOfCars(inventory))