const cars = require("../cars/cars")
const inventory = cars.inventory
const findCar = require("../problem1")


// Test 1 
let carId = 33
 let result = findCar(inventory,carId)
 console.log(result)

  // Test 2 
   
  console.log(findCar([],carId))
 
  // Test 3
 
  console.log(findCar(inventory,"22"))
  console.log(findCar(inventory,22.5))
  console.log(findCar(inventory,[22]))
 console.log(findCar([],"22"))
 console.log(findCar(inventory,0))
  //Test 4 

  console.log(findCar())

//  console.log(typeof(22.5) == "number")



